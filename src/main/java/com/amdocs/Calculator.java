package com.amdocs;

import java.util.logging.*; 

public class Calculator {
  private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

  public Calculator() {
	LOGGER.log(Level.FINE, "Constructing Calculator");
  }

  public int add() {
    return 3 + 6;
  
  }

  public int sub() {
	    return 6 - 3;
	  
	  }
  
  }
