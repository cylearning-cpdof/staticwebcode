package com.amdocs;

import java.util.logging.*; 

public class Increment {
	private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	private int counter = 1;
	
	public Increment() {
		LOGGER.log(Level.FINE, "Constructing Increment");
	}

	public int getCounter() {
	    return counter++;
	}
			
	public int decreasecounter(final int input) {
		if (input == 0){
	        counter--;
		}
		else if (input != 1) {
			counter++;
		}

		return counter;
	}			
}
