package com.amdocs;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import java.util.logging.*; 

public class IncrementTest {
	private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	private Increment inc;

	public IncrementTest() {
		LOGGER.log(Level.FINE, "Constructing Increment");
	}

	@Before
	public void setUp() {
		inc = new Increment();
	}

	@Test
	public void testGetCounterFirstCount() {
		final int firstCallCount = inc.getCounter();

		assertEquals("First call to getCounter() failed", 1, firstCallCount);
	}

	@Test
	public void testGetCounterSecondCount() {
		inc.getCounter();
		final int secondCallCount = inc.getCounter();

		assertEquals("Second call to getCounter() failed", 2, secondCallCount);
	}

	@Test
	public void testDecreasecounterGetCounter() {
		final int noChangeCount = inc.decreasecounter(1);

		assertEquals("Failed to get counter", 1, noChangeCount);
	}

	@Test
	public void testDecreasecounterDecreaseCounter() {
		inc.decreasecounter(1);
		final int decreaseCount = inc.decreasecounter(0);

		assertEquals("Failed to decrease counter", 0, decreaseCount);
	}

	@Test
	public void testDecreasecounterIncreaseCounter() {
		inc.decreasecounter(1);
		inc.decreasecounter(0);
		inc.decreasecounter(2);
		final int increaseCount = inc.decreasecounter(2);

		assertEquals("Failed to increase counter", 2, increaseCount);
	}
}

