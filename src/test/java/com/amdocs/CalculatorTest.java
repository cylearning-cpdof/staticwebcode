package com.amdocs;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import java.util.logging.*; 

public class CalculatorTest {
	private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	private Calculator calc;

	public CalculatorTest() {
		LOGGER.log(Level.FINE, "Constructing CalculatorTest");
	}

	@Before
	public void setUp() {
		calc = new Calculator();
	}

	@Test
	public void testAdd() {
		final int result = calc.add();
		assertEquals("Addition doesn't work", 9, result);
	}

	@Test
	public void testSub() {
		final int result = calc.sub();
		assertEquals("Subtraction doesn't work", 3, result);
	}
}
